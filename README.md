# joblist boards

This is a website (frontend) for [boards.joblist.today](https://boards.joblist.today) using [gitlab.com/sctlib/libli](https://gitlab.com/sctlib/libli) to display event.

## Overview

Possible to search companies recruiting worldwide, on a map.

## Deployment

- all the code is in `/index`
- all of it is moved to `/public` when deployed, which it is served by gitlab pages (index.html)
- deployed with `.gitlab-ci.yml`, `pages` job.
