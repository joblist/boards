import libli from "https://cdn.jsdelivr.net/npm/@sctlib/libli";
import joblist from "https://cdn.jsdelivr.net/npm/@joblist/components@0.0.11";

const $app = document.createElement("libli-app");
$app.setAttribute("origin", window.location.origin);
$app.setAttribute("pathname", window.location.pathname);
$app.setAttribute("hostname", "boards");
$app.setAttribute("homeserver-authorized", JSON.stringify(["*"]));
$app.setAttribute("hash", "#boards.joblist.today:matrix.org");
globalThis.document.querySelector("joblist-layout").append($app);
